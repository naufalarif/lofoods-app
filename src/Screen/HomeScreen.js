import React from 'react'
import { View, Text, StyleSheet, SafeAreaView } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

// component
import CategoryList from '../component/category/list/CategoryList'
import PopularList from '../component/category/list/PopularList'
import Search from '../component/Search'

// utils
import { Data, Popular } from '../static/database'

export default function HomeScreen() {
  /**
   * Only return components
   * logic not allowed below!
   */
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.search}>
            <Search />
          </View>
          <View style={styles.category}>
            <CategoryList payload={Data} />
          </View>
          <View style={styles.categoryTitle}>
            <Text style={styles.textTitle}>Popular</Text>
          </View>
          <View>
            <PopularList payload={Popular} />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 17
  },
  search: {
    marginBottom: 25
  },
  category: {
    marginBottom: 35
  },
  categoryTitle: {
    marginBottom: 15
  },
  textTitle: {
    color: '#222',
    fontWeight: 'bold',
    fontSize: 18
  }
});