import React, { useEffect, useState, useCallback } from 'react'
import { 
  StyleSheet, 
  Dimensions, 
  AsyncStorage, 
  SafeAreaView, 
  ScrollView, 
  RefreshControl 
} from 'react-native'
import isEmpty from 'lodash/isEmpty'

// component
import EmptyScreen from '../component/EmptyScreen'
import FavoriteList from '../component/category/list/FavoriteList'

export default function FavoriteScreen() {
  // hooks state setup
  const [favorite, setFavorite] = useState([])
  const [refresh, setRefresh] = useState(false)

  // create function here
  const wait = (timeout) => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout)
    })
  }

  const onRefresh = useCallback(() => {
    setRefresh(true)
    wait(2000).then(() => setRefresh(false))
  }, [])

  useEffect(() => {
    const updateFav = async () => {
      try {
        const getUser = JSON.parse(await AsyncStorage.getItem('user'))
        const fav = !isEmpty(getUser) ? getUser.fav : []
        setFavorite(fav)
      } catch (err) {
        console.log(err)
      }
    }
    
    updateFav()
  }, [refresh])

  // logic here
  const displayFavorite = !isEmpty(favorite) ? <FavoriteList payload={favorite} /> : <EmptyScreen />

  /**
   * Only return components
   * logic not allowed below!
   */
  return (
    <SafeAreaView>
      <ScrollView
        style={styles.container}
        refreshControl={<RefreshControl refreshing={refresh} onRefresh={onRefresh} />}
      >
        {displayFavorite}
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 17,
    backgroundColor: '#fff',
    height: Dimensions.get('screen').height,
  },
  list: {

  },
  textResult: {
    marginBottom: 20
  }
})
