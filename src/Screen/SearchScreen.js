import React from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import _ from 'lodash'

// utils
import { Search } from '../static/database'
import ChangeNameRestaurant from '../Utils/ChangeNameRestaurant'

// component
import SearchList from '../component/category/list/SearchList'

export default function SearchScreen({ route }) {
  // set data
  const { payload } = route.params
  const query = ChangeNameRestaurant(payload.toLowerCase())
  const arr = []
  Search.forEach(data => {
    if(_.isEqual(data.title, query)) {
      return arr.push(data)
    }
  })
  
  // logic here
  const displayCard = _.isEmpty(arr) ? <Text>Maaf restauran tidak ada</Text> : <SearchList payload={arr} />

  /**
   * Only return components
   * logic not allowed here!
   */
  return (
    <View style={styles.container}>
      <Text style={styles.textResult}>Result of {payload}...</Text>
      <View>
        {displayCard}
      </View>
    </View>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    padding: 17,
    backgroundColor: '#fff',
    height: Dimensions.get('screen').height
  },
  textResult: {
    marginBottom: 20
  }
})
