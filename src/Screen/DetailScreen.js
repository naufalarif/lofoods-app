import React, { useState, useEffect } from 'react'
import { 
  View, 
  Text, 
  ScrollView, 
  Image, 
  StyleSheet, 
  Linking, 
  Dimensions, 
  AsyncStorage, 
  TouchableOpacity 
} from 'react-native'
import isEmpty from 'lodash/isEmpty'

// Component
import MenuList from '../component/category/list/MenuList'

// Icon
import Ionicons from 'react-native-vector-icons/FontAwesome'
import { useSelector } from 'react-redux'

export default function DetailScreen({ route }) {
  // set Redux
  const userState = useSelector(state => state.UserState)
  
  // set State
  const [bookmark, setBookmark] = useState(false)
  const [user, setUser] = useState({})
  
  // set Data
  const { payload } = route.params
  const payloadUser = !isEmpty(userState) ? userState.data : {}

  // create function here
  useEffect(() => {
    const checkUser = async () => {
      try {
        let checkFav = []
        const getUser = JSON.parse(await AsyncStorage.getItem('user'))
        const fav = !isEmpty(getUser) ? getUser.fav : []
        fav.forEach((items) => {
          if (items.title === payload.title) {
            checkFav.push(items)
            setBookmark(true)
          }
        })
      } catch (err) {
        console.log(err)
      }
    }

    checkUser()
  }, [user])

  const handleLinking = () => {
    Linking.openURL(`https://www.google.com/maps/search/${payload.title}`)
  }

  const handleBookmark = async (payload) => {
    try {
      // store fav array
      let favStore = []
      // check equal fav
      let equalFav = []
      const getUser = JSON.parse(await AsyncStorage.getItem('user'))
      const fav = !isEmpty(getUser) ? getUser.fav : []
      // checking fav array
      fav.forEach((items) => {
        if (items.title === payload.title) {
          equalFav.push(items)
          favStore.push(items)
        } else {
          favStore.push(items)
        }
      })
  
      // do stuff here
      // if not added
      if (equalFav.length === 0) {
        const getUser = JSON.parse(await AsyncStorage.getItem('user'))
        const fav = !isEmpty(getUser) ? getUser.fav : []
        const favStore = fav.concat(payload)
        let data = {
          created: payloadUser.created,
          firstname: payloadUser.firstname,
          lastname: payloadUser.lastname,
          password: payloadUser.password,
          username: payloadUser.username,
          fav: favStore
        }
        const dataUser = JSON.stringify(data)
        await AsyncStorage.setItem('user', dataUser)
        setUser(dataUser)
        setBookmark(true)
      } else {
        // already added and remove recent fav
        // if only one left
        if (fav.length === 0) {
          let fav = []
          let data = {
            created: payloadUser.created,
            firstname: payloadUser.firstname,
            lastname: payloadUser.lastname,
            password: payloadUser.password,
            username: payloadUser.username,
            fav: fav
          }
          const dataUser = JSON.stringify(data)
          await AsyncStorage.setItem('user', dataUser)
          setUser(dataUser)
          setBookmark(false)
        } else {
          // if more than 2 fav
          let favStore = []
          const getUser = JSON.parse(await AsyncStorage.getItem('user'))
          const fav = !isEmpty(getUser) ? getUser.fav : []
          fav.forEach((items) => {
            if (items.title !== payload.title) {
              favStore.push(items)
            }
          })
          let data = {
            created: payloadUser.created,
            firstname: payloadUser.firstname,
            lastname: payloadUser.lastname,
            password: payloadUser.password,
            username: payloadUser.username,
            fav: favStore
          }
          const dataUser = JSON.stringify(data)
          await AsyncStorage.setItem('user', dataUser)
          setUser(dataUser)
          setBookmark(false)
        }
      }
    } catch (err) {
      console.log(err)
    }
  }

  // logic here
  const rating = payload.rating
  const ratingNum = rating.substring(0, 3)
  const ratingDesc = rating.substring(4, rating.length)
  const bookmarkIcon = 
    bookmark 
      ? <Ionicons name='bookmark' size={24} color={'#222'}/> 
      : <Ionicons name='bookmark-o' size={24} color={'#222'}/>

  /**
   * Only return components
   * logic not allowed below!
   */
  return (
    <ScrollView>
      <View style={styles.container}>
        <TouchableOpacity onPress={handleLinking}>
          <View>
            <Image 
              source={payload.map}
              style={styles.mapImg}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.wrapView}>
          <View style={styles.iconContainer}>
            <View style={styles.cardIcon}>
              <Image 
                source={payload.image}
                style={styles.icon}
              />
            </View>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.textTitle}>{payload.title}</Text>
            <Text style={styles.textDesc}>{payload.desc}</Text>
            <View style={styles.ratingContainer}>
              <Image 
                source={require('../../assets/icon/star.png')}
                style={styles.ratingImg}
              />
              <Text style={styles.ratingNum}>{ratingNum}</Text>
              <Text style={styles.ratingDesc}>{ratingDesc}</Text>
            </View>
          </View>
          <TouchableOpacity style={styles.iconFav} onPress={() => handleBookmark(payload)}>
            {bookmarkIcon}
          </TouchableOpacity>
        </View>
        <View style={styles.menuContainer}>
          <Text style={styles.textTitle}>Menu</Text>
          <MenuList payload={payload.menu} />
        </View>
      </View>
    </ScrollView>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    height: Dimensions.get('screen').height
  },
  wrapView: {
    marginTop: -50,
    backgroundColor: '#fff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flexDirection: 'row',
    paddingLeft: 30,
    paddingRight: 30
  },
  mapImg: {
    height: 200,
    width: undefined,
    resizeMode: 'cover'
  },
  iconContainer: {
    marginTop: -50,
  },
  cardIcon: {
    width: 110,
    padding: 5,
    backgroundColor: '#FAF3F1',
    borderRadius: 12
  }, 
  icon: {
    width: 100,
    height: 140,
    borderRadius: 12
  },
  textContainer: {
    paddingLeft: 15,
    paddingRight: 30,
    paddingTop: 10,
    paddingBottom: 40,
    width: '60%',
  },
  textTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#222',
    marginBottom: 5
  },
  textDesc: {
    color: '#747F88',
    marginBottom: 5,
    width: 175
  },
  ratingContainer: {
    flexDirection: 'row'
  },
  ratingImg: {
    width: 15,
    height: 15,
    marginTop: 2,
    marginRight: 5
  },
  ratingNum: {
    fontWeight: 'bold',
    marginRight: 5
  },
  ratingDesc: {
    color: '#747F88'
  },
  menuContainer: {
    padding: 30
  },
  iconFav: {
    marginLeft: 6,
    marginTop: 13
  }
})
