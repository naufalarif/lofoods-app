import React from 'react'
import { View, Image, StyleSheet, Text } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

// component
import DetailCategoryList from '../component/category/list/DetailCategoryList'

export default function DetailCategoryScreen({ route }) {
  // set data
  const { payload } = route.params

  /**
   * Only return components
   * logic not allowed here!
   */
  return (
      <ScrollView>
        <View >
          <View style={styles.imageContainer}>
            <Image 
              style={styles.image}
              source={payload.image}
            />
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.textTitle}>{payload.title}</Text>
          </View>
          <View style={styles.container}>
            <Text style={styles.textSubTitle}>{payload.results.length} Restaurants</Text>
            <DetailCategoryList payload={payload.results} />
            <Text style={styles.nomore}>no more</Text>
          </View>
        </View>
      </ScrollView>
    )
  
}

// style here!
const styles = StyleSheet.create({
  imageContainer: {
    marginBottom: 10
  },
  image: {
    width: undefined,
    height: 250,
    resizeMode: 'cover'
  },
  titleContainer: {
    paddingTop: 30,
    paddingBottom: 17,
    paddingLeft: 27,
    paddingRight: 27,
    marginTop: -130,
  },
  textTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#eaeaea'
  },
  container: {
    paddingTop: 30,
    paddingBottom: 17,
    paddingLeft: 27,
    paddingRight: 27,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30
  },
  textSubTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20
  },
  nomore: {
    textAlign: 'center',
    color: '#747F88'
  }
})
