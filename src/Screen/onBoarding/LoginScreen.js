import React, { useState } from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TextInput, 
  TouchableOpacity, 
  Image, 
  KeyboardAvoidingView, 
  ScrollView 
} from 'react-native'
import Ionicons from 'react-native-vector-icons/MaterialCommunityIcons'

// Redux
import { useDispatch, useSelector } from 'react-redux'
import * as ACTIONS from '../../Hooks/Actions/AuthAction'

export default function LoginScreen({ navigation }) {
  // Redux setup
  const dispatch = useDispatch()
  const loginRequest = authToken => dispatch(ACTIONS.loginRequest(authToken))
  const loginState = useSelector(state => state.AuthState)

  // hooks state setup
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [blocker, setBlocker] = useState(false)

  // create function here
  const inputs = {}

  const focusInput = (id) => {
    inputs[id].focus()
  }

  const handleLogin = () => {
    let data = {
      username: username,
      password: password
    }
    if(username && password) {
      loginRequest(data)
      setBlocker(false)
    } else {
      setBlocker(true)
    }
  }

  const handleNavigate = () => {
    setBlocker(false)
    navigation.navigate('Sign Up')
  }

  // put logic here!
  const blockerText = <Text style={styles.blocker}>{blocker ? 'Field must be filled!' : loginState.error ? `${loginState.error}` : null}</Text>

  /**
   * Only return components
   * logic not allowed here!
   */
  return (
    <KeyboardAvoidingView>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.bg}>
            <Image 
              source={require('../../../assets/image/background/junkfood.jpeg')}
              style={styles.imgBg}
            />
            <Text style={styles.title}>Lofoods</Text>
          </View>
          <View style={styles.formContainer}>
            <Text style={styles.txtWelcome}>Welcome</Text>
            
            <View style={blocker ? styles.blockerContainer : styles.inputContainer}>
              <View style={styles.inputForm}>
                <Ionicons 
                  name='account-card-details' 
                  size={16} 
                  color={'#b2b3cf'} 
                  style={styles.icon}
                />
                <TextInput 
                  style={styles.inputText}
                  placeholder='Username'
                  autoCapitalize='none'
                  onChangeText={text => setUsername(text)}
                  returnKeyType='next'
                  blurOnSubmit={false}
                  onSubmitEditing={() => { focusInput('field2') }}
                />
              </View>
              <View style={styles.inputForm}>
                <Ionicons 
                  name='key-variant' 
                  size={16} 
                  color={'#b2b3cf'} 
                  style={styles.icon}  
                />
                <TextInput 
                  style={styles.inputText}
                  placeholder='Password'
                  textContentType='password'
                  autoCapitalize='none'
                  secureTextEntry={true}
                  onChangeText={text => setPassword(text)}
                  returnKeyType='done'
                  ref={input => { inputs['field2'] = input }}
                />
              </View>
            </View>

            {blockerText}
            
            <TouchableOpacity style={styles.btnLogin} onPress={() => handleLogin()}>
              <Text style={styles.txtLogin}>Sign In</Text>
            </TouchableOpacity>

            <View>
              <Text style={styles.txtForget}>Forget password?</Text>
            </View>

            <TouchableOpacity style={styles.btnRegist} onPress={handleNavigate}>
              <Text style={styles.txtRegis}>Create Account</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}

// style here!
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  bg: {
    height: 200,
  },
  imgBg: {
    width: undefined,
    resizeMode: 'cover',
    height: 220
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: -190,
    paddingLeft: 35,
    color: '#fff'
  },  
  txtWelcome: {
    color: '#222',
    fontSize: 18,
    fontWeight: '700',
    marginBottom: 25
  }, 
  formContainer: {
    padding: 35,
    marginTop: -20,
    backgroundColor: '#fff',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40
  },
  inputContainer: {
    marginBottom: 40
  },
  blockerContainer: {
    marginBottom: 0
  },
  inputForm: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#e8e9ef',
    marginBottom: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 15
  },
  icon: {
    marginRight: 10,
    marginTop: 6
  },
  inputText: {
    color: '#222',
    width: '100%'
  },
  btnLogin: {
    backgroundColor: '#fff',
    alignItems: 'center',
    borderColor: '#ea4c89',
    borderWidth: 2,
    borderRadius: 15,
    paddingTop: 15,
    paddingBottom: 15,
    marginBottom: 10
  },
  txtLogin: {
    color: '#ea4c89'
  },
  txtForget: {
    marginBottom: 40,
    textAlign: 'center',
    color: '#b2b3cf'
  },
  btnRegist: {
    backgroundColor: '#ea4c89',
    alignItems: 'center',
    borderRadius: 15,
    paddingTop: 15,
    paddingBottom: 15
  },
  txtRegis: {
    color: '#fff'
  },
  blocker: {
    marginBottom: 19,
    color: '#d9534f'
  }
})
