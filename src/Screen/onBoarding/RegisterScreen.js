import React, { useState } from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TextInput, 
  TouchableOpacity, 
  Image, 
  KeyboardAvoidingView, 
  ScrollView, 
  SafeAreaView, 
  TouchableWithoutFeedback, 
  Keyboard 
} from 'react-native'
import Ionicons from 'react-native-vector-icons/MaterialCommunityIcons'
import { MonthNames } from '../../Utils/MonthNames'

// Redux
import { useDispatch } from 'react-redux'
import * as ACTION from '../../Hooks/Actions/AuthAction'

export default function RegisterScreen({ navigation }) {
  // Redux setup
  const dispatch = useDispatch()
  const registerRequest = data => dispatch(ACTION.registerRequest(data))

  // hooks state setup
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [first, setFirst] = useState('')
  const [last, setLast] = useState('')
  const [blocker, setBlocker] = useState(false)

  // create function here
  const inputs = {}

  const focusInput = (id) => {
    inputs[id].focus()
  }

  const handleSignUp = () => {
    const date = new Date()
    const monthIndex = date.getMonth()
    const monthName = MonthNames[monthIndex]
    const year = date.getFullYear()

    let data = {
      username: username,
      password: password,
      firstname: first,
      lastname: last,
      fav: [],
      created: `${monthName} ${year}`
    }
    if(username && password && first && last) {
      navigation.navigate('Sign In')
      registerRequest(data)
    } else {
      setBlocker(true)
    }
  }

  // put logic here!
  const blockerText = blocker ? <Text style={styles.blocker}>Field must be filled!</Text> : null

  /**
   * Only return components
   * logic not allowed here!
   */
  return (
    <KeyboardAvoidingView 
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
              <View style={styles.bg}>
                <Image 
                  source={require('../../../assets/image/background/junkfood.jpeg')}
                  style={styles.imgBg}
                />
                <Text style={styles.title}>Lofoods</Text>
              </View>
              <View style={styles.formContainer}>
                <Text style={styles.txtWelcome}>Create Account</Text>
                
                <View style={blocker ? styles.blockerInput : styles.inputContainer}>
                  <View style={styles.inputForm}>
                    <Ionicons 
                      name='account' 
                      size={16} 
                      color={'#b2b3cf'} 
                      style={styles.icon}
                    />
                    <TextInput 
                      style={styles.inputText}
                      returnKeyType='next'
                      placeholder='First Name'
                      blurOnSubmit={false}
                      onSubmitEditing={() => { focusInput('field2') }}
                      onChangeText={text => setFirst(text)}
                    />
                  </View>
                  <View style={styles.inputForm}>
                    <Ionicons 
                      name='account-plus' 
                      size={16} 
                      color={'#b2b3cf'} 
                      style={styles.icon}
                    />
                    <TextInput 
                      style={styles.inputText}
                      returnKeyType='next'
                      ref={input => { inputs['field2'] = input }}
                      onSubmitEditing={() => { focusInput('field3') }}
                      placeholder='Last Name'
                      onChangeText={text => setLast(text)}
                    />
                  </View>
                  <View style={styles.inputForm}>
                    <Ionicons 
                      name='account-card-details' 
                      size={16} 
                      color={'#b2b3cf'} 
                      style={styles.icon}
                    />
                    <TextInput 
                      style={styles.inputText}
                      returnKeyType='next'
                      placeholder='Username'
                      autoCapitalize='none'
                      ref={input => { inputs['field3'] = input }}
                      onSubmitEditing={() => { focusInput('field4') }}
                      onChangeText={text => setUsername(text)}
                    />
                  </View>
                  <View style={styles.inputForm}>
                    <Ionicons 
                      name='key-variant' 
                      size={16} 
                      color={'#b2b3cf'} 
                      style={styles.icon}  
                    />
                    <TextInput 
                      style={styles.inputText}
                      returnKeyType='done'
                      placeholder='Password'
                      textContentType='password'
                      autoCapitalize='none'
                      secureTextEntry={true}
                      ref={input => { inputs['field4'] = input }}
                      onChangeText={text => setPassword(text)}
                      
                    />
                  </View>
                </View>
                
                {blockerText}

                <TouchableOpacity style={styles.btnRegis} onPress={handleSignUp}>
                  <Text style={styles.txtRegis}>Sign Up</Text>
                </TouchableOpacity>

                <View style={styles.loginContainer}>
                  <Text style={styles.txt}>Already have an account? </Text>
                  <Text style={styles.txtLogin} onPress={() => navigation.navigate('Sign In')}> Sign in</Text>
                </View>
                <View style={{ flex: 1 }}/>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  )
}

// Style here!
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-end'
  },
  bg: {
    height: 200,
  },
  imgBg: {
    width: undefined,
    resizeMode: 'cover',
    height: 220
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: -190,
    paddingLeft: 35,
    color: '#fff'
  },  
  txtWelcome: {
    color: '#222',
    fontSize: 18,
    fontWeight: '700',
    marginBottom: 25
  }, 
  formContainer: {
    padding: 35,
    marginTop: -45,
    backgroundColor: '#fff',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40
  },
  inputContainer: {
    marginBottom: 35
  },
  blockerInput: {
    margin: 0
  },
  inputForm: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#e8e9ef',
    marginBottom: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 15
  },
  icon: {
    marginRight: 10,
    marginTop: 6
  },
  inputText: {
    color: '#222',
    width: '100%'
  },
  btnRegis: {
    backgroundColor: '#ea4c89',
    alignItems: 'center',
    borderRadius: 15,
    paddingTop: 15,
    paddingBottom: 10,
    marginBottom: 20
  },
  txtRegis: {
    color: '#fff'
  },
  loginContainer: {
    flexDirection: 'row',
    alignContent: 'center'
  },
  txt: {
    color: '#b2b3cf'
  },
  txtLogin: {
    // marginBottom: 45,
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#ea4c89'
  },
  blocker: {
    marginBottom: 16,
    color: '#d9534f'
  }
})
