import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import isEmpty from 'lodash/isEmpty'

// redux
import { useDispatch, useSelector } from 'react-redux'
import * as ACTION_LOGOUT from '../Hooks/Actions/AuthAction'
import * as ACTION_USER from '../Hooks/Actions/UserActions'

export default function ProfileScreen() {
  // redux setup
  const dispatch = useDispatch()
  const logoutRequest = () => dispatch(ACTION_LOGOUT.logoutRequest())
  const getUserRequest = () => dispatch(ACTION_USER.getUserRequest())
  const userState = useSelector(state => state.UserState)

  // set data
  const payload = !isEmpty(userState) ? userState.data : {}

  // create funtion here
  useEffect(() => {
    getUserRequest()
  }, [])

  const handleSignOut = () => {
    logoutRequest()
  }

  /**
   * Only return components
   * logic not allowed here!
   */
  return (
    <View style={styles.container}>
      <View>
        <Image 
          source={require('../../assets/bgprofile.jpg')}
          style={styles.bg}
        />
      </View>
      <View style={styles.imgContainer}>
        <Image 
          source={require('../../assets/icon/tomato.png')}
          style={styles.image}
        />
      </View>
      <View style={styles.profile}>
        <View>
          <Text style={styles.name}>{payload.username}</Text>
          <Text style={styles.desc}>Member since {payload.created}</Text>
          <TouchableOpacity style={styles.btnSignOut} onPress={() => handleSignOut()}>
            <Text style={styles.txtSignOut}>Sign Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    backgroundColor: '#fff',
  },
  bg: {
    resizeMode: 'cover',
    width: 360,
    height: 210
  },
  profile: {
    padding: 17,
  }, 
  imgContainer: {
    marginTop: -60,
    borderRadius: 60,
    backgroundColor: '#fff',
    padding: 7
  },
  image: {
    resizeMode: 'contain',
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  desc: {
    color: '#aeaeae'
  },
  btnSignOut: {
    backgroundColor: '#fff',
    alignItems: 'center',
    borderColor: '#ea4c89',
    borderWidth: 2,
    borderRadius: 15,
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 50
  },
  txtSignOut: {
    color: '#ea4c89'
  }
})
