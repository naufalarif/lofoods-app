export default function ChangeNameRestaurant(text) {
  // Fast food
  if(text === "mcd") { return "Mc Donald" }
  else if(text === "mc donald") { return "Mc Donald" }
  else if(text === "mcdonald") { return "Mc Donald" }
  else if(text === "kfc") { return "KFC" }
  else if(text === "burger king") { return "Burger King" }
  else if(text === "bk") { return "Burger King" }
  else if(text === "richeese") { return "Richeese Factory" }
  else if(text === "richeese factory") { return "Richeese Factory" }
  else if(text === "a&w") { return "A&W" }
  else if(text === "domino pizza") { return "Domino's Pizza" }
  else if(text === "dominos pizza") { return "Domino's Pizza" }
  else if(text === "domino's pizza") { return "Domino's Pizza" }
  else if(text === "pizza hut") { return "Pizza Hut" }
  // Japanese food
  else if(text === "sushi tei") { return "Sushi Tei" }
  else if(text === "sushitei") { return "Sushi Tei" }
  else if(text === "hanamasa") { return "Hanamasa" }
  else if(text === "marugame udon") { return "Marugame Udon" }
  else if(text === "shigeru") { return "Shigeru" }
  else if(text === "tom sushi") { return "Tom Sushi" }
  else if(text === "tomsushi") { return "Tom Sushi" }
  else if(text === "shabu yoi") { return "Shabu Yoi" }
  else if(text === "shabuyoi") { return "Shabu Yoi" }
  else if(text === "yoshinoya") { return "Yoshinoya" }
  else if(text === "genki sushi") { return "Genki Sushi" }
  else if(text === "genkisushi") { return "Genki Sushi" }
  else if(text === "hokben") { return "HokBen" }
  else if(text === "yuraku") { return "Yuraku" }
  else if(text === "gyukaku") { return "Gyu-Kaku Japanese BBQ" }
  else if(text === "gyu kaku") { return "Gyu-Kaku Japanese BBQ" }
  else if(text === "gyu-kaku") { return "Gyu-Kaku Japanese BBQ" }
  else if(text === "gyu kaku japanese bbq") { return "Gyu-Kaku Japanese BBQ" }
  else if(text === "gyukaku japanese bbq") { return "Gyu-Kaku Japanese BBQ" }
  else if(text === "gyu-kaku japanese bbq") { return "Gyu-Kaku Japanese BBQ" }
  // Seafood
  else if(text === "bandar djakarta") { return "Bandar Djakarta" }
  else if(text === "bandar jakarta") { return "Bandar Djakarta" }
  else if(text === "dcost") { return "D'Cost" }
  else if(text === "d'cost") { return "D'Cost" }
  else if(text === "fish & co.") { return "Fish & Co." }
  else if(text === "fish co") { return "Fish & Co." }
  else if(text === "fish n co") { return "Fish & Co." }
  else if(text === "fish n co.") { return "Fish & Co." }
  else if(text === "fish") { return "Fish & Co." }
  else if(text === "restoran 99 seafood") { return "Restoran 99 Seafood" }
  else if(text === "restoran 99") { return "Restoran 99 Seafood" }
  else if(text === "99 seafood") { return "Restoran 99 Seafood" }
  else if(text === "kapal laut resto") { return "Kapal Laut Resto" }
  else if(text === "kapal laut") { return "Restoran 99 Seafood" }
  else if(text === "restoran 99 seafood") { return "Pondok Pangandaran" }
  else if(text === "rm saung kepiting") { return "RM Saung Kepiting" }
  else if(text === "saung kepiting") { return "RM Saung Kepiting" }
  else if(text === "waroeng kampoeng seafood & ropang") { return "Waroeng Kampoeng Seafood & Ropang" }
  else if(text === "warung kampung seafood") { return "Waroeng Kampoeng Seafood & Ropang" }
  else if(text === "warung kampung seafood & ropang") { return "Waroeng Kampoeng Seafood & Ropang" }
  else if(text === "warung seafood") { return "Waroeng Kampoeng Seafood & Ropang" }
  else if(text === "kampung seafood") { return "Waroeng Kampoeng Seafood & Ropang" }
  // Korean food
  else if(text === "koba korean bbq") { return "KOBA Korean BBQ" }
  else if(text === "koba") { return "KOBA Korean BBQ" }
  else if(text === "koba korean") { return "KOBA Korean BBQ" }
  else if(text === "mujigae") { return "Mujigae" }
  else if(text === "kimo streetfood") { return "Kimo Streetfood" }
  else if(text === "kimo street food") { return "Kimo Streetfood" }
  else if(text === "kimo") { return "Kimo Streetfood" }
  else if(text === "kimo streetfood") { return "Kimo Streetfood" }
  else if(text === "seoul garden korean bbq") { return "Seoul Garden Korean BBQ"}
  else if(text === "seoul garden") { return "Seoul Garden Korean BBQ"}
  else if(text === "seoul") { return "Seoul Garden Korean BBQ"}
  else if(text === "seoul korean bbq") { return "Seoul Garden Korean BBQ"}
  else if(text === "kkumga korean bbq") { return "KKUMGA Korean BBQ" }
  else if(text === "kkumga") { return "KKUMGA Korean BBQ" }
  else if(text === "kkumga bbq") { return "KKUMGA Korean BBQ" }
  else if(text === "pochajjang") { return "Pochajjang" }
  else if(text === "pochajang") { return "Pochajjang" }
  else if(text === "gilgalbi korean bbq") { return "Gilgalbi Korean BBQ" }
  else if(text === "gilgalbi korean") { return "Gilgalbi Korean BBQ" }
  else if(text === "gilgalbi bbq") { return "Gilgalbi Korean BBQ" }
  else if(text === "mashiseo original korean bbq") { return "Mashiseo Original Korean BBQ" }
  else if(text === "mashiseo korean bbq") { return "Mashiseo Original Korean BBQ" }
  else if(text === "mashiseo bbq") { return "Mashiseo Original Korean BBQ" }
  else if(text === "mashiseo original korean") { return "Mashiseo Original Korean BBQ" }
  else if(text === "canaan resto") { return "Canaan Resto" }
  else if(text === "canaan") { return "Canaan Resto" }
  // Thai food
  else if(text === "thai street") { return "Thai STREET" }
  else if(text === "raa cha") { return "Raa Cha Suki & Barbeque" }
  else if(text === "racha") { return "Raa Cha Suki & Barbeque" }
  else if(text === "raacha") { return "Raa Cha Suki & Barbeque" }
  else if(text === "rachaa") { return "Raa Cha Suki & Barbeque" }
  else if(text === "raa cha suki") { return "Raa Cha Suki & Barbeque" }
  else if(text === "raacha suki") { return "Raa Cha Suki & Barbeque" }
  else if(text === "rachaa suki") { return "Raa Cha Suki & Barbeque" }
  else if(text === "racha suki") { return "Raa Cha Suki & Barbeque" }
  else if(text === "raa cha suki & barberque") { return "Raa Cha Suki & Barbeque" }
  else if(text === "raa cha suki & bbq") { return "Raa Cha Suki & Barbeque" }
}