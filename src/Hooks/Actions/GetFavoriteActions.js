import { AsyncStorage } from "react-native"
import isEmpty from "lodash/isEmpty"

export const GET_FAVORITE_REQUEST = 'GET_FAVORITELIST_REQUEST'
export const GET_FAVORITE_SUCCESS = 'GET_FAVORITELIST_SUCCESS'
export const GET_FAVORITE_FAILURE = 'GET_FAVORITELIST_FAILURE'

export const getFavoriteList = () => {
  return async dispatch => {
    try {
      const userData = JSON.parse(await AsyncStorage.getItem('user'))
      const fav = !isEmpty(userData) ? userData.fav : []
      dispatch(getFavoriteSuccess(fav))
    } catch (err) {
      dispatch(getFavoriteFailure(err))
    }
  }
}

export const getFavoriteSuccess = (fav) => {
  return {
    type: GET_FAVORITE_SUCCESS,
    payload: fav
  }
}

export const getFavoriteFailure = (err) => {
  return {
    type: GET_FAVORITE_FAILURE,
    err: err
  }
}