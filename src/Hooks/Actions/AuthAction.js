import { AsyncStorage } from "react-native"

export const GET_TOKEN_REQUEST = 'GET_TOKEN_REQUEST'
export const GET_TOKEN_SUCCESS = 'GET_TOKEN_SUCCESS'
export const GET_TOKEN_FAILURE = 'GET_TOKEN_FAILURE'

export const REGISTER_REQUEST = 'REGISTER_REQUEST'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAILURE = 'REGISTER_FAILURE'

export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE'

// token action
export const getTokenRequest = (data) => {
  return dispatch => {
    if(data !== null) {
      dispatch(getTokenSuccess(data))
    } else {
      dispatch(getTokenFailure('Token not available'))
    }
    return {
      type: GET_TOKEN_REQUEST
    }
  }
}

export const getTokenSuccess = (data) => {
  return {
    type: GET_TOKEN_SUCCESS,
    token: data
  }
}

export const getTokenFailure = (err) => {
  return {
    type: GET_TOKEN_FAILURE,
    error: err
  }
}

// register action
export const registerRequest = (data) => {
  return async dispatch => {
    try {
      const userData = JSON.stringify(data)
      await AsyncStorage.setItem('user', userData)
      dispatch(registerSuccess())
    } catch (err) {
      dispatch(registerFailure(err))
    }
    return {
      type: REGISTER_REQUEST
    }
  }
}

export const registerSuccess = () => {
  return {
    type: REGISTER_SUCCESS,
  }
}

export const registerFailure = (err) => {
  return {
    type: REGISTER_FAILURE,
    error: err
  }
}

// Login Action
export const loginRequest = (data) => {
  return async dispatch => {
    try {
      const userData = JSON.parse(await AsyncStorage.getItem('user'))
      if((data.username && data.password) === (userData.username && userData.password)) {
        await AsyncStorage.setItem('freshToken', 'dummy-token')
        const freshToken = await AsyncStorage.getItem('freshToken')
        dispatch(loginSuccess(freshToken))
      } else {
        dispatch(loginFailure('username or password incorrect!'))
        console.log('login failure')
      }
    } catch (err) {
      dispatch(loginFailure(err))
    }
    return {
      type: LOGIN_REQUEST
    }
  }
}

export const loginSuccess = (freshToken) => {
  return {
    type: LOGIN_SUCCESS,
    token: freshToken
  }
}

export const loginFailure = (err) => {
  return {
    type: LOGIN_FAILURE,
    error: err
  }
}

// logout action
export const logoutRequest = () => {
  return async dispatch => {
    try {
      await AsyncStorage.removeItem('freshToken')
      dispatch(logoutSuccess())
    } catch (err) {
      dispatch(logoutFailure(err))
    }
    return {
      type: LOGIN_REQUEST
    }
  }
}

export const logoutSuccess = () => {
  return {
    type: LOGOUT_SUCCESS
  }
}

export const logoutFailure = () => {
  return {
    type: LOGOUT_FAILURE
  }
}