import { AsyncStorage } from "react-native"

export const ADD_FAVORITE_REQUEST = 'ADD_FAVORITE_REQUEST'
export const ADD_FAVORITE_SUCCESS = 'ADD_FAVORITE_SUCCESS'
export const ADD_FAVORITE_FAILURE = 'ADD_FAVORITE_FAILURE'

export const addFavoriteRequest = (data) => {
  return async dispatch => {
    try {
      const userData = JSON.stringify(data)
      await AsyncStorage.setItem('user', userData)
      dispatch(addFavoriteSuccess(data))
    } catch (err) {
      console.log(err)
      dispatch(addFavoriteFailure(err))
    }
  }
}

export const addFavoriteSuccess = (data) => {
  return {
    type: ADD_FAVORITE_SUCCESS,
    payload: data
  }
}

export const addFavoriteFailure = (err) => {
  return {
    type: ADD_FAVORITE_FAILURE,
    err: err
  }
}