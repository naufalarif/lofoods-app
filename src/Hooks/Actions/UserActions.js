import { AsyncStorage } from "react-native"

export const GET_USER_REQUEST = 'GET_USER_REQUEST'
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS'
export const GET_USER_FAILURE = 'GET_USER_FAILURE'

export const getUserRequest = () => {
  return async dispatch => {
    try {
      const payload = JSON.parse(await AsyncStorage.getItem('user'))
      dispatch(getUserSuccess(payload))
    } catch (err) {
      dispatch(getUserFailure(err))
    }
  }
}

export const getUserSuccess = (data) => {
  return {
    type: GET_USER_SUCCESS,
    payload: data
  }
}

export const getUserFailure = (err) => {
  return {
    type: GET_USER_FAILURE,
    err: err
  }
}
