import * as ACTIONS from '../Actions/GetFavoriteActions'

export const INITIAL_STATE = {
  data: [],
  success: false,
  error: null,
  fetching: false
}

export default function getFavoriteReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTIONS.GET_FAVORITE_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false,
      }
    case ACTIONS.GET_FAVORITE_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        data: action.payload,
        error: false,
      }
    case ACTIONS.GET_FAVORITE_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.err,
        success: false,
      }
    default: return state
  }
}