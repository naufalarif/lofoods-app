import * as ACTIONS from '../Actions/UserActions'

export const INITIAL_STATE = {
  data: {},
  success: false,
  error: null,
  fetching: false
}

export default function userReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTIONS.GET_USER_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTIONS.GET_USER_SUCCESS:
      return {
        ...state,
        data: action.payload,
        fetching: false,
        success: true,
        error: false,
      }
    case ACTIONS.GET_USER_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.err
      }
    default: return state
  }
}
