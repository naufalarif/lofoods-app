import * as ACTIONS from '../Actions/FavoriteAction'

export const INITIAL_STATE = {
  data: {},
  success: false,
  error: null,
  fetching: false,
}

export default function favoriteReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTIONS.ADD_FAVORITE_REQUEST:
      return {
        ...state,
        success: false,
        error: false,
        fetching: true
      }
    case ACTIONS.ADD_FAVORITE_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        data: action.payload,
        fetching: false
      }
    case ACTIONS.ADD_FAVORITE_FAILURE:
      return {
        ...state,
        success: false,
        error: action.err,
        fetching: false
      }
    default: return state
  }
} 