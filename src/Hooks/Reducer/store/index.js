import { combineReducers } from 'redux'
import authReducer from '../AuthReducer'
import favoriteReducer from '../FavoriteReducer'
import getFavoriteReducer from '../GetFavoriteReducer'
import userReducer from '../UserReducer'

const rootReducer = combineReducers({
  AuthState: authReducer,
  FavoriteState: favoriteReducer,
  FavoriteListState: getFavoriteReducer,
  UserState: userReducer
})

export default rootReducer