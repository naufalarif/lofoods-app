import { useReducer } from "react";
import * as ACTION from '../Actions/AuthAction'

const INITIAL_STATE = {
  success: false,
  error: false,
  fetching: false,
  token: null
}

export default function authReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ACTION.GET_TOKEN_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION.GET_TOKEN_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        error: false,
        token: action.token
      }
    case ACTION.GET_TOKEN_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.error,
      }
    case ACTION.REGISTER_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION.REGISTER_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        error: false
      }
    case ACTION.REGISTER_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.error.err
      }
    case ACTION.LOGIN_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION.LOGIN_SUCCESS:
      return {
        ...state,
        token: action.token,
        fetching: false,
        success: true,
        error: false,
      }
    case ACTION.LOGIN_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.error
      }
    case ACTION.LOGOUT_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION.LOGOUT_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        error: false,
        token: null
      }
    case ACTION.LOGOUT_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.error.err
      }
    default: return state
  }
}
