import React from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import isEmpty from 'lodash/isEmpty'

// stateless component
export default function CardFavorite({ payload }) {
  const navigation = useNavigation()

  // create function here
  const _onPress = () => {
    navigation.navigate('Detail', {
      payload: payload
    })
  }
  
  // logic here
  const rating = !isEmpty(payload) ? payload.rating : ''
  const ratingNum = rating.substring(0, 3)
  const ratingDesc = rating.substring(4, rating.length)

  /**
   * Only return components
   * logic not allowed below!
   */
  return (
    <TouchableOpacity onPress={_onPress}>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image 
            source={payload.image}
            style={styles.image}
          />
        </View>
      <View style={styles.textContainer}>
        <Text style={styles.title}>{payload.title}</Text>
          <Text style={styles.desc}>{payload.desc}</Text>
          <View style={styles.ratingContainer}>
            <Image 
              source={require('../../../../assets/icon/star.png')}
              style={styles.ratingImg}
            />
            <Text style={styles.ratingNum}>{ratingNum}</Text>
            <Text style={styles.ratingDesc}>{ratingDesc}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 10,
    marginBottom: 20,
    backgroundColor: '#FAF3F1'
  },
  imageContainer: {
    marginRight: 15,
    padding: 10,
    borderRadius: 10,
  },
  image: {
    width: 90,
    height: 90,
    borderRadius: 10,
  },
  textContainer: {
    width: 150,
    paddingTop: 10,
    paddingBottom: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#222',
    marginBottom: 5
  },
  desc: {
    color: '#747F88',
    marginBottom: 5
  },
  ratingContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  ratingImg: {
    width: 15,
    height: 15,
    marginTop: 2,
    marginRight: 5
  },
  ratingNum: {
    fontWeight: 'bold',
    marginRight: 5
  },
  ratingDesc: {
    color: '#747F88'
  }
})
