import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

// stateless component
export default function CardPopular({ payload }) {
  const navigation = useNavigation()

  // create function here
  const _onPress = () => {
    navigation.navigate('Detail', {
      payload: payload
    })
  }

  /**
   * Only return components
   * logic not allowed below!
   */
  return (
    <TouchableOpacity onPress={_onPress}>
      <View style={styles.container}>
        <View>
          <View style={styles.containerImage}>
            <Image 
              style={styles.image}
              source={payload.image}
            />
          </View>
          <View style={styles.containerText}>
            <Text style={styles.textTitle}>{payload.title}</Text>
          </View>
          <View>
            <Text style={styles.textDesc}>{payload.desc}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    marginBottom: 25
  },
  containerImage: {
    borderRadius: 10,
    marginBottom: 10
  },
  image: {
    flex: 1,
    width: undefined,
    height: 150,
    borderRadius: 10,
    resizeMode: 'cover'
  },
  containerText: {
    marginBottom: 2
  },
  textTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#222'
  },
  textDesc: {
    color: '#747F88'
  }
})
