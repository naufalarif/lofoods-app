import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

// stateless component
export default function CardMenu({ payload }) {
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>{payload.name}</Text>
    </View>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderRadius: 10,
    marginBottom: 20,
    backgroundColor: '#FAF3F1'
  },
  textTitle: {
    color: '#222'
  }
})
