import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

// stateless component
export default function CardCategory({ payload, idx }) {
  const navigation = useNavigation()

  // create function here
  const handleNavigation = () => {
    navigation.navigate('Category', {
      payload: payload
    })
  }

  /**
   * Only return components
   * logic not allowed below!
   */
  return (
    <TouchableOpacity onPress={handleNavigation} key={idx}>
      <View style={styles.container}>
        <View style={styles.containerImage}>
          <Image 
            style={styles.image}
            source={payload.icon}
          />
        </View>
        <View style={styles.cardText}>
          <Text style={styles.text}>{payload.title}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    width: 90,
    height: 100,
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerImage: {
    padding: 10,
    height: 65,
    width: 80,
    marginBottom: 5,
    borderRadius: 12,
    backgroundColor: '#FEAB8B',
    alignItems: 'center'
  },
  image: {
    width: 40,
    height: 40,
    resizeMode: 'cover'
  },
  cardText: {
    
  },
  text: {
    color: '#747F88',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})
