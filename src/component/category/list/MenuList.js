import React from 'react'
import { View, FlatList } from 'react-native'

// component
import CardMenu from '../card/CardMenu'

// stateless component
export default function MenuList({ payload }) {
  return (
    <View>
      <FlatList 
        data={payload}
        renderItem={({ item, index }) => <CardMenu payload={item} key={index} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}
