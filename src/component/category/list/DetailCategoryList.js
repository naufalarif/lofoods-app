import React from 'react'
import { FlatList } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native'

// component
import CardDetailCategory from '../card/CardDetailCategory'

// stateless component
export default function DetailCategoryList({ payload }) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList 
        data={payload}
        renderItem={({ item, index }) => 
          <CardDetailCategory 
            payload={item} 
            key={index}
          />
        }
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  )
}
