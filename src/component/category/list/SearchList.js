import React from 'react'
import { FlatList, View } from 'react-native'

// component
import CardSearch from '../card/CardSearch'

// stateless component
export default function SearchList({ payload }) {
  return (
    <View>
      <FlatList
        data={payload}
        renderItem={(item) => <CardSearch data={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}
