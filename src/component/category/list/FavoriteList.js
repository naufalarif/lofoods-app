import React from 'react'
import { FlatList, SafeAreaView } from 'react-native'

// component
import CardFavorite from '../card/CardFavorite'

// stateless component
export default function FavoriteList({ payload }) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList 
        data={payload}
        renderItem={({ item }) => <CardFavorite payload={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  )
}
