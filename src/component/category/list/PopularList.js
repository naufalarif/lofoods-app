import React from 'react'
import { FlatList, SafeAreaView } from 'react-native'

// component
import CardPopular from '../card/CardPopular'

// stateless component
export default function PopularList({ payload }) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList
        data={payload}
        renderItem={({ item }) => <CardPopular payload={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  )
}
