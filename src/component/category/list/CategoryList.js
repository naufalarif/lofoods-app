import React from 'react'
import { FlatList, SafeAreaView } from 'react-native'

// component
import CardCategory from '../card/CardCategory'

// stateless component
export default function CategoryList({ payload }) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList 
        horizontal={true}
        data={payload}
        renderItem={({ item }) => <CardCategory payload={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  )
}
