import React, { useState } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import _ from 'lodash'

export default function Search() {
  const navigation = useNavigation()

  // hooks state setup
  const [query, setQuery] = useState('')

  // create function here
  const _onPress = () => {
    if(!_.isEmpty(query)) {
      navigation.navigate('Search', {
        payload: query
      })
    }
    setQuery('')
  }

  /**
   * Only return components
   * logic not allowed below!
   */
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={_onPress}>
        <Image 
          source={require('../../assets/icon/search.png')}
          style={styles.imageSearch}
        />
      </TouchableOpacity>
      <TextInput 
        placeholder='Search foods'
        placeholderTextColor='#747F88'
        style={styles.textInput}
        onChangeText={text => setQuery(text)}
      />
    </View>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: '#E0E0E0',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  imageSearch: {
    width: 20,
    height: 20,
    marginRight: 15,
    marginTop: 4
  },
  textInput: {
    color: '#747F88',
    fontWeight: 'bold',
    width: '100%'
  }
})
