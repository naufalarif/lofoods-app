import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

// stateless component
export default function EmptyScreen() {
  return (
    <View style={styles.container}>
      <Image 
        source={require('../../assets/empty.png')}
        style={styles.image}
      />
      <Text style={styles.text}>Belum ada favorit</Text>
    </View>
  )
}

// style here
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 17,
  },
  image: {
    resizeMode: 'contain',
    width: 150,
    height: 150
  },
  text: {
    color: '#aeaeae'
  }
})
