export const IconCategory = {
  FastIcon: require('../../assets/icon/junk.png'),
  SeaIcon: require('../../assets/icon/sea.png'),
  JapanIcon: require('../../assets/icon/japan.png'),
  ThaiIcon: require('../../assets/icon/thailand.png'),
  KoreanIcon: require('../../assets/icon/korean.png')
}

export const ImageDetailCategory = {
  FastFood: require('../../assets/image/background/junkfood.jpeg'),
  SeaFood: require('../../assets/image/background/seafood.jpeg'),
  JapanFood: require('../../assets/image/background/japanfood.jpeg'),
  ThaiFood: require('../../assets/image/background/thaifood.png'),
  Koreanfood: require('../../assets/image/background/koreanfood.jpg')
}

export const ImagePopular = {
  RaachaImage: require('../../assets/raacha.jpg')
}

export const ImageFastFood = {
  Mcd: require('../../assets/image/fastfood/mcd.jpg'),
  Kfc: require('../../assets/image/fastfood/kfc.jpg'),
  Aw: require('../../assets/image/fastfood/aw.png'),
  Bk: require('../../assets/image/fastfood/burgerking.jpg'),
  Dominos: require('../../assets/image/fastfood/dominos.jpg'),
  Pizzahut: require('../../assets/image/fastfood/pizzahut.jpg'),
  Richeese: require('../../assets/image/fastfood/richeese.jpg'),
}

export const ImageJapanFood = {
  Genki: require('../../assets/image/japanfood/genki.jpg'),
  Gyukaku: require('../../assets/image/japanfood/gyukaku.jpg'),
  Hanamasa: require('../../assets/image/japanfood/hanamasa.jpg'),
  Hokben: require('../../assets/image/japanfood/hokben.jpg'),
  Marugame: require('../../assets/image/japanfood/marugame.jpg'),
  Shabuyoi: require('../../assets/image/japanfood/shabuyoi.jpg'),
  Shigeru: require('../../assets/image/japanfood/shigeru.jpg'),
  Sushitei: require('../../assets/image/japanfood/sushitei.jpg'),
  Tomsushi: require('../../assets/image/japanfood/tomsushi.jpg'),
  Yoshinoya: require('../../assets/image/japanfood/yoshinoya.jpg'),
  Yuraku: require('../../assets/image/japanfood/yuraku.jpg'),
}

export const ImageSeaFood = {
  Bandar: require('../../assets/image/seafood/bandardjakarta.jpg'),
  Dcost: require('../../assets/image/seafood/dcost.jpg'),
  Fishnco: require('../../assets/image/seafood/fishnco.jpg'),
  Kampong: require('../../assets/image/seafood/kampongseafood.jpg'),
  Kapallaut: require('../../assets/image/seafood/kapallaut.jpg'),
  Pangandaran: require('../../assets/image/seafood/pndkpangandaran.jpg'),
  Saungkepiting: require('../../assets/image/seafood/saungkepiting.jpg'),
  Seafood99: require('../../assets/image/seafood/seafood99.jpg'),
}

export const ImageKoreanFood = {
  Canaan: require('../../assets/image/koreanfood/canaan.jpg'),
  Gilgalbi: require('../../assets/image/koreanfood/gilgalbi.jpg'),
  Kimo: require('../../assets/image/koreanfood/kimo.jpg'),
  Kkumga: require('../../assets/image/koreanfood/kkumga.jpg'),
  Koba: require('../../assets/image/koreanfood/koba.jpg'),
  Mujigae: require('../../assets/image/koreanfood/mujigae.jpg'),
  Pochajjang: require('../../assets/image/koreanfood/pochajjang.jpg'),
  Soulgarden: require('../../assets/image/koreanfood/soulgarden.jpg'),
}

export const ImageThaiFood = {
  Rachaa: require('../../assets/image/thaifood/raacha.jpg'),
  Thaistreet: require('../../assets/image/thaifood/thaistreet.jpg')
}

export const ImageMaps = {
  // Fast food
  Mcd: require('../../assets/image/maps/mcd.png'),
  Kfc: require('../../assets/image/maps/kfc.png'),
  Aw: require('../../assets/image/maps/aw.png'),
  Bk: require('../../assets/image/maps/bk.png'),
  Dominos: require('../../assets/image/maps/dominos.png'),
  Pizzahut: require('../../assets/image/maps/pizzahut.png'),
  Richeese: require('../../assets/image/maps/richeese.png'),

  // Japan food
  Genki: require('../../assets/image/maps/genki.png'),
  Gyukaku: require('../../assets/image/maps/gyukaku.png'),
  Hanamasa: require('../../assets/image/maps/hanamasa.png'),
  Hokben: require('../../assets/image/maps/hokben.png'),
  Marugame: require('../../assets/image/maps/marugame.png'),
  Shabuyoi: require('../../assets/image/maps/shabuyoi.png'),
  Shigeru: require('../../assets/image/maps/shigeru.png'),
  Sushitei: require('../../assets/image/maps/sushitei.png'),
  Tomsushi: require('../../assets/image/maps/tomsushi.png'),
  Yoshinoya: require('../../assets/image/maps/yoshinoya.png'),
  Yuraku: require('../../assets/image/maps/yuraku.png'),

  // Korean food
  Canaan: require('../../assets/image/maps/canaan.png'),
  Gilgalbi: require('../../assets/image/maps/gilgalbi.png'),
  Kimo: require('../../assets/image/maps/kimo.png'),
  Kkumga: require('../../assets/image/maps/kkumga.png'),
  Koba: require('../../assets/image/maps/koba.png'),
  Mujigae: require('../../assets/image/maps/mujigae.png'),
  Pochajjang: require('../../assets/image/maps/pochajjang.png'),
  Soulgarden: require('../../assets/image/maps/soulgarden.png'),

  // Sea food
  Bandar: require('../../assets/image/maps/bandardjakarta.png'),
  Dcost: require('../../assets/image/maps/dcost.png'),
  Fishnco: require('../../assets/image/maps/fishnco.png'),
  Kampong: require('../../assets/image/maps/kampong.png'),
  Kapallaut: require('../../assets/image/maps/kapallaut.png'),
  Pangandaran: require('../../assets/image/maps/pangandaran.png'),
  Saungkepiting: require('../../assets/image/maps/saungkepiting.png'),
  Seafood99: require('../../assets/image/maps/seafood99.png'),

  // thai food
  Rachaa: require('../../assets/image/maps/raacha.png'),
  Thaistreet: require('../../assets/image/maps/thaistreet.png')
}