import { 
  IconCategory, 
  ImageDetailCategory, 
  ImageFastFood, 
  ImageJapanFood,
  ImageSeaFood,
  ImageKoreanFood,
  ImageThaiFood,
  ImageMaps
} from './images'

export const Data = [
  {
    title: 'Fast Food',
    icon: IconCategory.FastIcon,
    image: ImageDetailCategory.FastFood,
    results: [
      {
        title: 'Mc Donald',
        desc: 'Classic - burgers - fries & shakes.',
        rating: '4.4 (3,931 reviews)',
        image: ImageFastFood.Mcd,
        map: ImageMaps.Mcd,
        menu: [
          {
            name: 'Ayam McD Special Sambal Bawang'
          },
          {
            name: 'Ayam McD Sambal Bawang'
          },
          {
            name: 'Chicken & Rise Special With Cheese'
          },
          {
            name: 'PaNas 1'
          },
          {
            name: 'PaNas 2'
          },
          {
            name: 'PaNas Special'
          },
          {
            name: 'Cheeseburger'
          },
          {
            name: 'Beef Burger'
          },
          {
            name: 'Big Mac'
          },
          {
            name: 'Fanta McFloat'
          },
          {
            name: 'Coca-Cola McFloat'
          },
          {
            name: 'Fanta'
          },
          {
            name: 'Coca Cola'
          },

        ]
      },
      {
        title: 'KFC',
        desc: 'Buckets of fried chicken - wings - sides.',
        rating: '4.4 (4,847 reviews)',
        image: ImageFastFood.Kfc,
        map: ImageMaps.Kfc,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Burger King',
        desc: 'Grilled burgers - fries & shakes - breakfast.',
        rating: '4.4 (1,944 reviews',
        image: ImageFastFood.Bk,
        map: ImageMaps.Bk,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Richeese Factory',
        desc: 'Casual - Good for kids',
        rating: '4.3 (2,030 reviews)',
        image: ImageFastFood.Richeese,
        map: ImageMaps.Richeese,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'A&W',
        desc: 'Late-night food - Casual - Good for kids',
        rating: '4.4 (1,881 reviews)',
        image: ImageFastFood.Aw,
        map: ImageMaps.Aw,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: "Domino's Pizza",
        desc: 'Pizzas & a variety of other dishes & sides.',
        rating: '4.3 (1,348 reviews)',
        image: ImageFastFood.Dominos,
        map: ImageMaps.Dominos,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: "Pizza Hut",
        desc: 'Burger - Friench frise - Breakfast',
        rating: '4.5 (1,361 reviews)',
        image: ImageFastFood.Pizzahut,
        map: ImageMaps.Pizzahut,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
    ]
  },
  {
    title: 'Japan Food',
    icon: IconCategory.JapanIcon,
    image: ImageDetailCategory.JapanFood,
    results: [
      {
        title: 'Sushi Tei',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.6 (3,355 reviews)',
        image: ImageJapanFood.Sushitei,
        map: ImageMaps.Sushitei,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Hanamasa',
        desc: 'Cosy - Casual - Vegetarian options',
        rating: '4.6 (1,136 reviews)',
        image: ImageJapanFood.Hanamasa,
        map: ImageMaps.Hanamasa,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Marugame Udon',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.5 (2,850 reviews)',
        image: ImageJapanFood.Marugame,
        map: ImageMaps.Marugame,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Shigeru',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.4 (1,153 reviews)',
        image: ImageJapanFood.Shigeru,
        map: ImageMaps.Shigeru,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Tom Sushi',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.5 (896 reviews)',
        image: ImageJapanFood.Tomsushi,
        map: ImageMaps.Tomsushi,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Shabu Yoi',
        desc: 'Reservations required - All you can eat - Cosy',
        rating: '4.3 (397 reviews)',
        image: ImageJapanFood.Shabuyoi,
        map: ImageMaps.Shabuyoi,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Yoshinoya',
        desc: 'Fast-food-style - rice bowls with beef - veggies & more - plus soups & salads.',
        rating: '4.4 (1,271 reviews)',
        image: ImageJapanFood.Yoshinoya,
        map: ImageMaps.Yoshinoya,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Genki Sushi',
        desc: 'Diners pluck sushi & maki rolls from a moving conveyor belt at this Japan-based chain outpost.',
        rating: '4.4 (889 reviews)',
        image: ImageJapanFood.Genki,
        map: ImageMaps.Genki,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'HokBen',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.3 (3,147 reviews)',
        image: ImageJapanFood.Hokben,
        map: ImageMaps.Hokben,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Yuraku',
        desc: 'All you can eat - Cosy - Casual',
        rating: '4.6 (754 reviews)',
        image: ImageJapanFood.Yuraku,
        map: ImageMaps.Yuraku,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Gyu-Kaku Japanese BBQ',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.6 (1,175 reviews)',
        image: ImageJapanFood.Gyukaku,
        map: ImageMaps.Gyukaku,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
    ]
  },
  {
    title: 'Sea Food',
    icon: IconCategory.SeaIcon,
    image: ImageDetailCategory.SeaFood,
    results: [
      {
        title: 'Bandar Djakarta',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.4 (8,314 reviews)',
        image: ImageSeaFood.Bandar,
        map: ImageMaps.Bandar,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: "D'Cost",
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.3 (568 reviews)',
        image: ImageSeaFood.Dcost,
        map: ImageMaps.Dcost,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Fish & Co.',
        desc: 'Burger - Friench fries - Breakfast',
        rating: '4.5 (263 reviews)',
        image: ImageSeaFood.Fishnco,
        map: ImageMaps.Fishnco,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Restoran 99 Seafood',
        desc: 'Cash only - Cosy - Casual',
        rating: '4.7 (70 reviews)',
        image: ImageSeaFood.Seafood99,
        map: ImageMaps.Seafood99,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Kapal Laut Resto',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.1 (635 reviews)',
        image: ImageSeaFood.Kapallaut,
        map: ImageMaps.Kapallaut,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Pondok Pangandaran',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.4 (982 reviews)',
        image: ImageSeaFood.Pangandaran,
        map: ImageMaps.Pangandaran,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'RM Saung Kepiting',
        desc: 'Late-night food - Cosy - Casual',
        rating: '4.3 (628 reviews)',
        image: ImageSeaFood.Saungkepiting,
        map: ImageMaps.Saungkepiting,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Waroeng Kampoeng Seafood & Ropang',
        desc: 'Cosy - Casual - Good for kids',
        rating: '4.3 (592 reviews)',
        image: ImageSeaFood.Kampong,
        map: ImageMaps.Kampong,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
    ]
  },
  {
    title: 'Korean Food',
    icon: IconCategory.KoreanIcon,
    image: ImageDetailCategory.Koreanfood,
    results: [
      {
        title: 'KOBA Korean BBQ',
        desc: 'Contemporary fixture with a comfortable indoor/outdoor seating area serving Korean BBQ.',
        rating: '4.4 (313 reviews)',
        image: ImageKoreanFood.Koba,
        map: ImageMaps.Koba,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Mujigae',
        desc: 'Cosy · Casual · Good for kids',
        rating: '4.5 (2,397 ratings)',
        image: ImageKoreanFood.Mujigae,
        map: ImageMaps.Mujigae,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Kimo Streetfood',
        desc: 'Cosy · Casual · Good for kids',
        rating: '4.8 (121 ratings)',
        image: ImageKoreanFood.Kimo,
        map: ImageMaps.Kimo,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Seoul Garden Korean BBQ',
        desc: 'Cosy · Casual · Good for kids',
        rating: '4.2 (40 ratings)',
        image: ImageKoreanFood.Soulgarden,
        map: ImageMaps.Soulgarden,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'KKUMGA Korean BBQ',
        desc: 'All you can eat · Cosy · Casual',
        rating: '4.5 (155 ratings)',
        image: ImageKoreanFood.Kkumga,
        map: ImageMaps.Kkumga,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Pochajjang',
        desc: 'Lofty, contemporary fixture turning out hearty portions of classic Korean barbecue & sides.',
        rating: '4.2 (441 reviews)',
        image: ImageKoreanFood.Pochajjang,
        map: ImageMaps.Pochajjang,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Gilgalbi Korean BBQ',
        desc: 'Cosy · Casual · Good for kids',
        rating: '4.6 (148 reviews)',
        image: ImageKoreanFood.Gilgalbi,
        map: ImageMaps.Gilgalbi,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Canaan Resto',
        desc: 'Cosy · Casual · Good for kids',
        rating: '4.2 (34 reviews)',
        image: ImageKoreanFood.Canaan,
        map: ImageMaps.Canaan,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
    ]
  },
  {
    title: 'Thai Food',
    icon: IconCategory.ThaiIcon,
    image: ImageDetailCategory.ThaiFood,
    results: [
      {
        title: 'Thai STREET',
        desc: 'Cosy · Casual · Good for kids',
        rating: '4.5 (1,509 ratings)',
        image: ImageThaiFood.Thaistreet,
        map: ImageMaps.Thaistreet,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
      {
        title: 'Raa Cha Suki & Barbeque',
        desc: 'Cosy · Casual · Good for kids',
        rating: '4.4 (1,753 ratings)',
        image: ImageThaiFood.Rachaa,
        map: ImageMaps.Rachaa,
        menu: [
          {
            name: 'Chicken',
          },
          {
            name: 'Desserts'
          },
          {
            name: 'Drinks'
          },
          {
            name: 'Burger'
          },
          {
            name: 'Breakfast'
          },
          {
            name: 'Sides'
          }
        ]
      },
    ]
  },
]

export const Popular = [
  {
    title: 'Mc Donald',
    desc: 'Classic - burgers - fries & shakes.',
    rating: '4.4 (3,931 reviews)',
    image: ImageFastFood.Mcd,
    map: ImageMaps.Mcd,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Yoshinoya',
    desc: 'Fast-food-style - rice bowls with beef - veggies & more - plus soups & salads.',
    rating: '4.4 (1,271 reviews)',
    image: ImageJapanFood.Yoshinoya,
    map: ImageMaps.Yoshinoya,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Bandar Djakarta',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.4 (8,314 reviews)',
    image: ImageSeaFood.Bandar,
    map: ImageMaps.Bandar,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Mujigae',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.5 (2,397 ratings)',
    image: ImageKoreanFood.Mujigae,
    map: ImageMaps.Mujigae,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Raa Cha Suki & Barbeque',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.4 (1,753 ratings)',
    image: ImageThaiFood.Rachaa,
    map: ImageMaps.Rachaa,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
]

export const Search = [
  {
    title: 'Thai STREET',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.5 (1,509 ratings)',
    image: ImageThaiFood.Thaistreet,
    map: ImageMaps.Thaistreet,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Raa Cha Suki & Barbeque',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.4 (1,753 ratings)',
    image: ImageThaiFood.Rachaa,
    map: ImageMaps.Rachaa,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Mc Donald',
    desc: 'Classic - burgers - fries & shakes.',
    rating: '4.4 (3,931 reviews)',
    image: ImageFastFood.Mcd,
    map: ImageMaps.Mcd,
    menu: [
      {
        name: 'Ayam McD Special Sambal Bawang'
      },
      {
        name: 'Ayam McD Sambal Bawang'
      },
      {
        name: 'Chicken & Rise Special With Cheese'
      },
      {
        name: 'PaNas 1'
      },
      {
        name: 'PaNas 2'
      },
      {
        name: 'PaNas Special'
      },
      {
        name: 'Cheeseburger'
      },
      {
        name: 'Beef Burger'
      },
      {
        name: 'Big Mac'
      },
      {
        name: 'Fanta McFloat'
      },
      {
        name: 'Coca-Cola McFloat'
      },
      {
        name: 'Fanta'
      },
      {
        name: 'Coca Cola'
      },

    ]
  },
  {
    title: 'KFC',
    desc: 'Buckets of fried chicken - wings - sides.',
    rating: '4.4 (4,847 reviews)',
    image: ImageFastFood.Kfc,
    map: ImageMaps.Kfc,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Burger King',
    desc: 'Grilled burgers - fries & shakes - breakfast.',
    rating: '4.4 (1,944 reviews',
    image: ImageFastFood.Bk,
    map: ImageMaps.Bk,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Richeese Factory',
    desc: 'Casual - Good for kids',
    rating: '4.3 (2,030 reviews)',
    image: ImageFastFood.Richeese,
    map: ImageMaps.Richeese,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'A&W',
    desc: 'Late-night food - Casual - Good for kids',
    rating: '4.4 (1,881 reviews)',
    image: ImageFastFood.Aw,
    map: ImageMaps.Aw,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: "Domino's Pizza",
    desc: 'Pizzas & a variety of other dishes & sides.',
    rating: '4.3 (1,348 reviews)',
    image: ImageFastFood.Dominos,
    map: ImageMaps.Dominos,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: "Pizza Hut",
    desc: 'Burger - Friench frise - Breakfast',
    rating: '4.5 (1,361 reviews)',
    image: ImageFastFood.Pizzahut,
    map: ImageMaps.Pizzahut,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Sushi Tei',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.6 (3,355 reviews)',
    image: ImageJapanFood.Sushitei,
    map: ImageMaps.Sushitei,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Hanamasa',
    desc: 'Cosy - Casual - Vegetarian options',
    rating: '4.6 (1,136 reviews)',
    image: ImageJapanFood.Hanamasa,
    map: ImageMaps.Hanamasa,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Marugame Udon',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.5 (2,850 reviews)',
    image: ImageJapanFood.Marugame,
    map: ImageMaps.Marugame,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Shigeru',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.4 (1,153 reviews)',
    image: ImageJapanFood.Shigeru,
    map: ImageMaps.Shigeru,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Tom Sushi',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.5 (896 reviews)',
    image: ImageJapanFood.Tomsushi,
    map: ImageMaps.Tomsushi,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Shabu Yoi',
    desc: 'Reservations required - All you can eat - Cosy',
    rating: '4.3 (397 reviews)',
    image: ImageJapanFood.Shabuyoi,
    map: ImageMaps.Shabuyoi,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Yoshinoya',
    desc: 'Fast-food-style - rice bowls with beef - veggies & more - plus soups & salads.',
    rating: '4.4 (1,271 reviews)',
    image: ImageJapanFood.Yoshinoya,
    map: ImageMaps.Yoshinoya,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Genki Sushi',
    desc: 'Diners pluck sushi & maki rolls from a moving conveyor belt at this Japan-based chain outpost.',
    rating: '4.4 (889 reviews)',
    image: ImageJapanFood.Genki,
    map: ImageMaps.Genki,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'HokBen',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.3 (3,147 reviews)',
    image: ImageJapanFood.Hokben,
    map: ImageMaps.Hokben,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Yuraku',
    desc: 'All you can eat - Cosy - Casual',
    rating: '4.6 (754 reviews)',
    image: ImageJapanFood.Yuraku,
    map: ImageMaps.Yuraku,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Gyu-Kaku Japanese BBQ',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.6 (1,175 reviews)',
    image: ImageJapanFood.Gyukaku,
    map: ImageMaps.Gyukaku,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Bandar Djakarta',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.4 (8,314 reviews)',
    image: ImageSeaFood.Bandar,
    map: ImageMaps.Bandar,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: "D'Cost",
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.3 (568 reviews)',
    image: ImageSeaFood.Dcost,
    map: ImageMaps.Dcost,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Fish & Co.',
    desc: 'Burger - Friench fries - Breakfast',
    rating: '4.5 (263 reviews)',
    image: ImageSeaFood.Fishnco,
    map: ImageMaps.Fishnco,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Restoran 99 Seafood',
    desc: 'Cash only - Cosy - Casual',
    rating: '4.7 (70 reviews)',
    image: ImageSeaFood.Seafood99,
    map: ImageMaps.Seafood99,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Kapal Laut Resto',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.1 (635 reviews)',
    image: ImageSeaFood.Kapallaut,
    map: ImageMaps.Kapallaut,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Pondok Pangandaran',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.4 (982 reviews)',
    image: ImageSeaFood.Pangandaran,
    map: ImageMaps.Pangandaran,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'RM Saung Kepiting',
    desc: 'Late-night food - Cosy - Casual',
    rating: '4.3 (628 reviews)',
    image: ImageSeaFood.Saungkepiting,
    map: ImageMaps.Saungkepiting,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Waroeng Kampoeng Seafood & Ropang',
    desc: 'Cosy - Casual - Good for kids',
    rating: '4.3 (592 reviews)',
    image: ImageSeaFood.Kampong,
    map: ImageMaps.Kampong,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'KOBA Korean BBQ',
    desc: 'Contemporary fixture with a comfortable indoor/outdoor seating area serving Korean BBQ.',
    rating: '4.4 (313 reviews)',
    image: ImageKoreanFood.Koba,
    map: ImageMaps.Koba,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Mujigae',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.5 (2,397 ratings)',
    image: ImageKoreanFood.Mujigae,
    map: ImageMaps.Mujigae,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Kimo Streetfood',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.8 (121 ratings)',
    image: ImageKoreanFood.Kimo,
    map: ImageMaps.Kimo,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Seoul Garden Korean BBQ',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.2 (40 ratings)',
    image: ImageKoreanFood.Soulgarden,
    map: ImageMaps.Soulgarden,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'KKUMGA Korean BBQ',
    desc: 'All you can eat · Cosy · Casual',
    rating: '4.5 (155 ratings)',
    image: ImageKoreanFood.Kkumga,
    map: ImageMaps.Kkumga,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Pochajjang',
    desc: 'Lofty, contemporary fixture turning out hearty portions of classic Korean barbecue & sides.',
    rating: '4.2 (441 reviews)',
    image: ImageKoreanFood.Pochajjang,
    map: ImageMaps.Pochajjang,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Gilgalbi Korean BBQ',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.6 (148 reviews)',
    image: ImageKoreanFood.Gilgalbi,
    map: ImageMaps.Gilgalbi,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
  {
    title: 'Canaan Resto',
    desc: 'Cosy · Casual · Good for kids',
    rating: '4.2 (34 reviews)',
    image: ImageKoreanFood.Canaan,
    map: ImageMaps.Canaan,
    menu: [
      {
        name: 'Chicken',
      },
      {
        name: 'Desserts'
      },
      {
        name: 'Drinks'
      },
      {
        name: 'Burger'
      },
      {
        name: 'Breakfast'
      },
      {
        name: 'Sides'
      }
    ]
  },
]
