import React, { useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { AsyncStorage } from 'react-native'
import 'react-native-gesture-handler'

// Icon
import Ionicons from 'react-native-vector-icons/MaterialCommunityIcons'

// Screen
import HomeScreen from './Screen/HomeScreen'
import DetailCategoryScreen from './Screen/DetailCategoryScreen'
import DetailScreen from './Screen/DetailScreen'
import SearchScreen from './Screen/SearchScreen'
import ProfileScreen from './Screen/ProfileScreen'
import FavoriteScreen from './Screen/FavoriteScreen'
import LoginScreen from './Screen/onBoarding/LoginScreen'
import RegisterScreen from './Screen/onBoarding/RegisterScreen'

// Redux Setup
import * as ACTION from './Hooks/Actions/AuthAction'
import { useSelector, useDispatch } from 'react-redux'


// Stack
const Tab = createBottomTabNavigator()
const HomeStack = createStackNavigator()
const ProfileStack = createStackNavigator()
const FavStack = createStackNavigator()
const LoginStack = createStackNavigator()

// Create Stack Screen here
function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name='Home' component={HomeScreen} />
      <HomeStack.Screen name='Category' component={DetailCategoryScreen} />
      <HomeStack.Screen name='Detail' component={DetailScreen} />
      <HomeStack.Screen name='Search' component={SearchScreen} />
    </HomeStack.Navigator>
  )
}

function ProfileStackScreen() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen name='Profile' component={ProfileScreen} />
    </ProfileStack.Navigator>
  )
}

function FavStackScreen() {
  return (
    <FavStack.Navigator>
      <FavStack.Screen name='Favorite' component={FavoriteScreen} />
    </FavStack.Navigator>
  )
}

// Bottom Navigation here
export default function BottomNav() {
  const dispath = useDispatch()
  const authToken = token => dispath(ACTION.getTokenRequest(token))
  const authState = useSelector(state => state.AuthState)

  useEffect(() => {
    const getAuthToken = async () => {
      try {
        const userToken = await AsyncStorage.getItem('freshToken')
        if(userToken) {
          authToken(userToken)
        }
      } catch (err) {
        console.log('err: ', err)
      }
    }

    getAuthToken()
  }, [])

  return (
    <NavigationContainer>
      {authState.token !== null ? (
          <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName
                if (route.name === "Home") {
                  iconName = `home${focused ? '' : '-outline'}`
                } else if (route.name === "Favorite") {
                  iconName = `heart${focused ? '' : '-outline'}`
                } else if (route.name === "Profile") {
                  iconName = `account-circle${focused ? '' : '-outline'}`
                }
                  return <Ionicons name={iconName} size={size} color={color} />
                },
              })}
              tabBarOptions={{
                activeTintColor: '#ea4c89',
                inactiveTintColor: 'gray',
                labelStyle: {
                fontWeight: 'bold',
                fontSize: 12,
              },
            }}>
            <Tab.Screen name="Home" component={HomeStackScreen} />
            <Tab.Screen name="Favorite" component={FavStackScreen} />
            <Tab.Screen name="Profile" component={ProfileStackScreen} />
          </Tab.Navigator>
        ): (
          <LoginStack.Navigator>
            <LoginStack.Screen name='Sign In' component={LoginScreen} />
            <LoginStack.Screen name='Sign Up' component={RegisterScreen} />
          </LoginStack.Navigator>
        )
      }
    </NavigationContainer>
  )
}
